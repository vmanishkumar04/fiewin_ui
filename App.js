import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Invite from './src/Components/Invite/Invite';
import Home from './src/Components/Home/Home';
import Recharge from './src/Components/Recharge/Recharge';
import Profile from './src/Components/Profile/Profile';
import {Image} from 'react-native';
import {HOME, ME, INVITE, RECHARGE} from './src/Constants/Images.constant';
import About from './src/Components/Profile/About/About';
import Login from './src/Components/Users/Login';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Fast from './src/Components/Home/game/Fast_Parity';
import Mine from './src/Components/Home/game/MineSweeper';
import Andar from './src/Components/Home/game/Andar_Bahar';
import Crash from './src/Components/Home/game/Crash';
import Dice from './src/Components/Home/game/Dice';
import Hilo from './src/Components/Home/game/Hilo';
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

export function HomeTabs() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        gestureEnabled: true,
        animation: 'slide_from_right',
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: () => (
            <Image source={HOME} style={{width: 30, height: 20}} />
          ),
        }}
      />
      <Tab.Screen
        name="Invite"
        component={Invite}
        options={{
          tabBarIcon: () => (
            <Image source={INVITE} style={{width: 30, height: 20}} />
          ),
        }}
      />
      <Tab.Screen
        name="Recharge"
        component={Recharge}
        options={{
          tabBarIcon: () => (
            <Image source={RECHARGE} style={{width: 30, height: 20}} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarIcon: () => (
            <Image source={ME} style={{width: 30, height: 20}} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
export default function App() {
  const data = async () => {
    const res = await AsyncStorage.getItem('login');
    return res;
  };
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          gestureEnabled: true,
          animation: 'slide_from_right',
        }}>
        {/* <Stack.Screen name="Login" component={Login} /> */}
        <Stack.Screen name="Root" component={HomeTabs} />
        <Stack.Screen name="About_Us" component={About} />
        {/* gaming screen */}
        <Stack.Screen name="Fast" component={Fast} />
        <Stack.Screen name="Mine" component={Mine} />
        <Stack.Screen name="Andar" component={Andar} />
        <Stack.Screen name="Crash" component={Crash} />
        <Stack.Screen name="Dice" component={Dice} />
        <Stack.Screen name="Hilo" component={Hilo} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
