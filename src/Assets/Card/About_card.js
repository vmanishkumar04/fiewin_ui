import {Image, Text, View} from 'react-native';
import React from 'react';
import About_css from '../../Utilities/Css/About_css';
const About_card = ({data}) => {
  return (
    <View style={About_css.cardContainer}>
      <View style={About_css.cardView}>
        <Image
          source={data.source}
          style={{height: 50, width: 50, borderRadius: 30}}
        />
        <Text style={About_css.cardText1}>{data.title1}</Text>
        <Text style={About_css.cardText2}>{data.title2}</Text>
      </View>
      <View style={About_css.imgData_map}>
        {data.imgData.map((data, index) => {
          return (
            <View key={index} style={About_css.imgDataInner}>
              <Image source={data.source} style={{height: 110, width: 110}} />
              <Text style={About_css.imgDataInnerText}>{data.title4}</Text>
            </View>
          );
        })}
      </View>
    </View>
  );
};

export default About_card;
