import {
  ABOUT_1,
  ABOUT_2,
  ABOUT_3,
  ABOUT_4,
  ABOUT_5,
  ABOUT_6,
  ABOUT_2_1,
  ABOUT_2_2,
  ABOUT_2_3,
  ABOUT_2_4,
  ABOUT_3_1,
  ABOUT_3_2,
  ABOUT_3_3,
  ABOUT_3_4,
  ABOUT_4_1,
  ABOUT_4_2,
  ABOUT_4_3,
  ABOUT_4_4,
  ABOUT_5_1,
  ABOUT_5_2,
  ABOUT_5_3,
  ABOUT_5_4,
  ABOUT_7_1,
  ABOUT_0,
  ABOUT_0_1,
  ABOUT_0_2,
} from '../../Constants/Images.constant';
export const headerData = [
  {title1: 'Total Users', title2: '5,000.000 +', img: ABOUT_0},
  {title1: 'Total investors', title2: '900.000 +', img: ABOUT_0_1},
  {title1: 'All User income', title2: '7,50,000.000 +', img: ABOUT_0_2},
];

export const bodyCard_Hottest = [
  {
    title2: 'FAST',
    title3: ' ipsum dolor sit amet consectetur adipisicing .',
    img: ABOUT_1,
  },
  {
    title2: 'MINE',
    title3: ' ipsum dolor sit amet consectetur adipisicing .',
    img: ABOUT_2,
  },
  {
    title2: 'DICE',
    title3: ' ipsum dolor sit amet consectetur adipisicing .',
    img: ABOUT_3,
  },
  {
    title2: 'CRASH',
    title3: ' ipsum dolor sit amet consectetur adipisicing .',
    img: ABOUT_4,
  },
  {
    title2: 'ANDAR BAHAR',
    title3: ' ipsum dolor sit amet consectetur adipisicing .',
    img: ABOUT_5,
  },
  {
    title2: 'HILO',
    title3: 'ipsum dolor sit amet consectetur adipisicing ',
    img: ABOUT_6,
  },
];

export const restBodyData = [
  {
    title1: 'The Most Trust Worthy',
    title2: 'Lorem ipsum dolor sit amet consectetur adipisicing .',

    source: ABOUT_2_1,
    imgData: [
      {
        source: ABOUT_2_2,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_2_3,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_2_4,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
    ],
  },
  {
    title1: 'Top Service',
    title2: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
    source: ABOUT_3_1,
    imgData: [
      {
        source: ABOUT_3_2,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_3_3,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_3_4,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
    ],
  },
  {
    title1: 'The best way to make money',
    title2: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
    source: ABOUT_4_1,
    imgData: [
      {
        source: ABOUT_4_2,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_4_3,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_4_4,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
    ],
  },
  {
    title1: 'Proffesional Agency System',
    title2: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
    source: ABOUT_5_1,
    imgData: [
      {
        source: ABOUT_5_2,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_5_3,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
      {
        source: ABOUT_5_4,
        title4: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
      },
    ],
  },
];

export const footerData = [
  {
    data: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
    source: ABOUT_7_1,
  },
  {
    data: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
    source: ABOUT_7_1,
  },
];
