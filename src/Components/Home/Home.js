import {Text, View, Image, TouchableOpacity, FlatList} from 'react-native';
import React, {useEffect, useState} from 'react';
import Home_CSS from '../../Utilities/Css/Home.css';
import {
  Task,
  Check,
  Fast,
  Mine,
  Andar,
  Crash,
  Dice,
  Hilo,
  PR_LOGO,
} from '../../Constants/Images.constant';
import {getFetch} from '../../api/api';
import {GAMELIST} from '../../Constants/ApiConstant';
const Home = ({navigation}) => {
  useEffect(() => {
    const fetchList = async () => {
      try {
        const data = await getFetch(GAMELIST, 'NO');
        console.log('hi', data);
        seCardData(data.message);
      } catch (error) {
        console.log('fail');
        return alert('No Games avaliable');
      }
    };
    fetchList();
  }, []);
  const amount = '2000 ';
  const id = '12344';
  const [CardData, seCardData] = useState([]);
  const getGame = data => {
    if (data == 'Fast') {
      return navigation.navigate('Fast');
    } else if (data == 'Mine') {
      return navigation.navigate('Mine');
    } else if (data == 'Andar Bahar') {
      return navigation.navigate('Andar');
    } else if (data == 'Crash') {
      return navigation.navigate('Crash');
    } else if (data == 'Dice') {
      return navigation.navigate('Dice');
    } else if (data == 'Hillo') {
      return navigation.navigate('Hilo');
    }
  };
  return (
    <>
      <View style={{flex: 1}}>
        {/* news */}
        <View style={Home_CSS.news}>
          <Image
            style={{height: 30, width: 30, borderRadius: 30, marginRight: '4%'}}
            source={PR_LOGO}
          />
          <Text style={Home_CSS.newsText}>
            <Text style={Home_CSS.newsText}>***1234 Wins </Text>
            <Text style={{...Home_CSS.newsText, color: 'green'}}>
              &#8377; {amount}
            </Text>
            <Text style={Home_CSS.newsText}>in Dice Game</Text>
          </Text>
        </View>

        {/* recharge and withdraw */}
        <View style={Home_CSS.recharge}>
          {/* point text */}
          <View style={Home_CSS.rechargeTextView}>
            <Text style={Home_CSS.textColor}> Point</Text>
            <Text style={Home_CSS.rechargeText}>0 rupees</Text>
            <Text style={Home_CSS.textColor}> ID:{id}</Text>
          </View>

          {/* buttons */}
          <View style={Home_CSS.btnView}>
            <TouchableOpacity
              style={Home_CSS.btnTextTouch}
              onPress={() => navigation.navigate('Recharge')}>
              <Text style={{...Home_CSS.btnText, color: '#ffffff'}}>
                Recharge
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{...Home_CSS.btnTextTouch, backgroundColor: '#b2b0b0db'}}
              onPress={() => navigation.navigate('Recharge')}>
              <Text style={Home_CSS.btnText}>Withdraw</Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* task and check in */}
        <View style={Home_CSS.taskContainer}>
          <TouchableOpacity style={Home_CSS.task}>
            <View style={Home_CSS.taskImg}>
              <Image style={{height: 50, width: 50}} source={Task} />
            </View>
            <Text style={Home_CSS.taskText}>Task Reward</Text>
          </TouchableOpacity>
          <TouchableOpacity style={Home_CSS.task}>
            <View style={Home_CSS.taskImg}>
              <Image style={{height: 50, width: 50}} source={Check} />
            </View>
            <Text style={Home_CSS.taskText}>Check In</Text>
          </TouchableOpacity>
        </View>
        {/* cards */}
      </View>
      <View
        style={{
          flexDirection: 'row',
          height: '60%',
        }}>
        <FlatList
          keyExtractor={(item, index) => index.toString()}
          data={CardData}
          numColumns={2}
          renderItem={({item, index}) => {
            console.log(item.img);
            return (
              <TouchableOpacity
                style={Home_CSS.cardElement}
                onPress={() => getGame(item.name)}>
                <Image
                  source={item.img.replace(/['"]+/g)}
                  style={{height: '100%', width: '100%'}}
                />
                <Text
                  style={{
                    color: '#ffffff',
                    top: -35,
                    textAlign: 'center',
                    fontWeight: '700',
                    fontSize: 17,
                  }}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            );
          }}
        />
      </View>
    </>
  );
};
export default Home;
