import {Text, View, TouchableOpacity, Image, ScrollView} from 'react-native';
import React, {useState} from 'react';
import Fast_Css from '../../../Utilities/Css/Fast_css';
import {ROCKET} from '../../../Constants/Images.constant';
import Fast_Modal from '../game_modal/fast/Fast_Modal';
import Fast_Rule from '../game_modal/fast/Fast_Rule';
import Chart from '../game_modal/common/Chart';
const Fast_Parity = ({navigation}) => {
  const [color, setColor] = useState(true);
  const [css, setCss] = useState(true);
  const [modal, setModal] = useState(false);
  const [sendData, setSendData] = useState();
  const [ruleModal, setRuleModal] = useState(false);
  const [sendRuleData, setSendRuleData] = useState();
  const ModalData = data => {
    setModal(true);
    setSendData(data);
  };
  const boxData = [
    {
      text: 'Green',
      color: '#00c283',
      no: '1:2',
      bid: '1.2',
    },
    {
      text: 'Voilet',
      color: '#6555d2',
      no: '1:2:4',
      bid: '1.24',
    },
    {
      text: 'Red',
      color: '#fa3c08',
      no: '1:4',
      bid: '1.4',
    },
  ];
  const numberData = [
    {
      data: 1,
      bid: '1:9',
    },
    {
      data: 2,
      bid: '1:9',
    },
    {
      data: 3,
      bid: '1:9',
    },
    {
      data: 4,
      bid: '1:9',
    },
    {
      data: 5,
      bid: '1:9',
    },
    {
      data: 6,
      bid: '1:9',
    },
    {
      data: 7,
      bid: '1:9',
    },
    {
      data: 8,
      bid: '1:9',
    },
  ];
  const circleData = [
    {n1: 111, n2: 1, color: '#00c283'},
    {n1: 111, n2: 1, color: '#fa3c08'},
    {n1: 111, n2: 1, color: '#00c283'},
    {n1: 111, n2: 1, color: '#6555d2'},
    {n1: 111, n2: 1, color: '#fa3c08'},
    {n1: 111, n2: 1, color: '#fa3c08'},
    {n1: 111, n2: 1, color: '#fa3c08'},
    {n1: 111, n2: 1, color: '#6555d2'},
    {n1: 111, n2: 1, color: '#00c283'},
    {n1: 111, n2: 1, color: '#fa3c08'},
    {n1: 111, n2: 1, color: '#00c283'},
    {n1: 111, n2: 1, color: '#6555d2'},
    {n1: 111, n2: 1, color: '#fa3c08'},
    {n1: 111, n2: 1, color: '#00c283'},
    {n1: 111, n2: 1, color: '#6555d2'},
  ];
  const tableHead = [
    {data: 'Period', width: '32%'},
    {data: 'User Select', width: '32%'},
    {data: 'Point', width: '32%'},
  ];
  const tableData = [
    {n1: 1234567, n2: 5000, n3: 134, n4: 12, color: '#00c283', width: '32%'},
    {n1: 1234567, n2: 5000, n3: 134, n4: 12, color: '#6555d2', width: '32%'},
    {n1: 1234567, n2: 5000, n3: 134, n4: 12, color: '#fa3c08', width: '32%'},
    {n1: 1234567, n2: 5000, n3: 134, n4: 12, color: '#00c283', width: '32%'},
    {n1: 1234567, n2: 5000, n3: 134, n4: 12, color: '#6555d2', width: '32%'},
  ];
  const nav = () => {
    navigation.navigate('Recharge');
  };
  return (
    <View style={{flex: 1}}>
      <Fast_Modal modal={modal} setModal={setModal} send={sendData} nav={nav} />
      <Fast_Rule
        modal={ruleModal}
        setModal={setRuleModal}
        send={sendRuleData}
      />
      <View style={Fast_Css.head}>
        <Text
          style={{...Fast_Css.color, width: '10%'}}
          onPress={() => navigation.goBack()}>
          {'<'}
        </Text>
        <View style={Fast_Css.btnView}>
          <TouchableOpacity
            style={css ? Fast_Css.headColor : Fast_Css.btn_Touch}
            onPress={() => setCss(!css)}>
            <Text style={css ? Fast_Css.headText : Fast_Css.btn_text}>
              Fast-Parity
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={!css ? Fast_Css.headColor : Fast_Css.btn_Touch}
            onPress={() => setCss(!css)}>
            <Text style={!css ? Fast_Css.headText : Fast_Css.btn_text}>
              Parity
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          {/* <Image/> */}
          <Text style={Fast_Css.color} onPress={() => setRuleModal(true)}>
            Rule
          </Text>
        </View>
      </View>
      {/* body start */}
      <ScrollView>
        {/* sec 1 */}
        <View style={Fast_Css.sec1}>
          <View>
            <Text style={{color: 'rgba(0,0,0,0.7)', fontWeight: '500'}}>
              Period
            </Text>
            <Text
              style={{color: 'rgb(0,0,0)', fontWeight: '900', fontSize: 18}}>
              123456789
            </Text>
          </View>
          <View style={Fast_Css.mainC}>
            <Text style={{color: 'rgba(0,0,0,0.7)', fontWeight: '500'}}>
              Count Down
            </Text>
            <View style={Fast_Css.count_V}>
              <Text style={Fast_Css.count_T}>0</Text>
              <Text style={Fast_Css.count_T}>0</Text>
              <Text style={{color: 'rgba(0,0,0,0.7)', marginHorizontal: '2%'}}>
                :
              </Text>
              <Text style={Fast_Css.count_T}>0</Text>
              <Text style={Fast_Css.count_T}>0</Text>
            </View>
          </View>
        </View>
        {/* sec 2*/}
        <View style={Fast_Css.mainSec2}>
          {boxData.map((data, index) => {
            return (
              <TouchableOpacity
                style={Fast_Css.sec2}
                key={index}
                onPress={() => ModalData(data)}>
                <View
                  style={{
                    ...Fast_Css.sec2_V,
                    backgroundColor: data.color,
                  }}>
                  <Image source={ROCKET} />
                  <Text style={{fontWeight: '800', color: '#ffffff'}}>
                    Join {data.text}
                  </Text>
                </View>
                <Text style={{color: 'rgba(0,0,0,0.7)'}}>{data.no}</Text>
              </TouchableOpacity>
            );
          })}
        </View>
        {/* sec 3 */}
        <View style={Fast_Css.sec3}>
          {numberData.map((data, index) => {
            return (
              <TouchableOpacity
                key={index}
                style={{
                  borderWidth: 0.5,
                  borderColor: '#cccccc',
                  width: '20%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: '3%',
                  borderRadius: 5,
                  margin: '2%',
                  backgroundColor: 'rgba(9,9,9,0.1)',
                }}
                onPress={() => ModalData(data)}>
                <Text
                  style={{fontSize: 18, color: '#000000', fontWeight: '500'}}>
                  {data.data}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
        <Text style={{color: '#000000', alignSelf: 'center'}}>
          {numberData[0].bid}
        </Text>
        {/* sec 4 */}
        <View
          style={{
            marginTop: '9%',
          }}>
          {/* head */}
          <View style={Fast_Css.head_4}>
            <TouchableOpacity
              style={{
                ...Fast_Css.t_4,
                borderBottomColor: color ? 'rgb(0,147,255)' : 'transparent',
              }}
              onPress={() => {
                setColor(!color);
              }}>
              <Text style={Fast_Css.t_4_t}>Continous</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                ...Fast_Css.t_4,
                borderBottomColor: !color ? 'rgb(0,147,255)' : 'transparent',
              }}
              onPress={() => {
                setColor(!color);
              }}>
              <Text style={Fast_Css.t_4_t}>Records</Text>
            </TouchableOpacity>
          </View>
          {/* body */}
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                padding: '5%',
              }}>
              <Text style={{color: '#000000', fontSize: 16, fontWeight: '600'}}>
                Fast Parity Record
              </Text>
              <Text style={{color: '#000000'}}> more {'>'}</Text>
            </View>
            <View
              style={{
                paddingHorizontal: '5%',
                flexWrap: 'wrap',
                flexDirection: 'row',
              }}>
              {circleData.map((data, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    style={{width: '10%', margin: '2%'}}>
                    <Text
                      style={{
                        color: '#000000',
                        textAlign: 'center',
                      }}>
                      {data.n1}
                    </Text>
                    <Text
                      style={{...Fast_Css.c_t, backgroundColor: data.color}}>
                      {data.n2}
                    </Text>
                  </TouchableOpacity>
                );
              })}
            </View>
          </View>
          {/* chart */}
          {/* sec 5 */}
          <Chart tableData={tableData} tableHead={tableHead} type="FAST" />
        </View>
      </ScrollView>
    </View>
  );
};

export default Fast_Parity;
