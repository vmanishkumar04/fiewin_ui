import {Text, View, TouchableOpacity, ScrollView} from 'react-native';
import React, {useState} from 'react';
import Mine_Css from '../../../Utilities/Css/Mine_Css';
import Chart from '../game_modal/common/Chart';
import Mine_Modal from '../game_modal/Mine/Modal';
import Mine_Rule from '../game_modal/Mine/Mine_Rule';
const MineSweeper = ({navigation}) => {
  const fakeData = [1, 2, 3, 4];
  const fakeData1 = [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4];
  const [modal, setModal] = useState(false);
  const [modalRule, setModalRule] = useState(false);
  const fakeData2 = [
    1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1,
    2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2,
    3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4,
  ];
  const tableHead = [
    {data: 'Time', width: '24%'},
    {data: 'User', width: '17%'},
    {data: 'Game', width: '17%'},
    {data: 'Pass', width: '17%'},
    {data: 'Bonus', width: '24%'},
  ];
  const tableData = [
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
    {
      n1: 1234567,
      n2: 5000,
      n3: 134,
      n4: 12,
      n5: '2 x 2',
      color: '#6555d2',
      width: '32%',
    },
  ];
  const [cardW, setCardW] = useState({
    width: '48%',
    height: '48%',
    title: fakeData,
    margin: '1%',
  });
  const nav = () => {
    return navigation.navigate('Recharge');
  };
  return (
    <View>
      <Mine_Modal modal={modal} setModal={setModal} nav={nav} />
      <Mine_Rule modal={modalRule} setModal={setModalRule} />
      {/* head */}
      <View style={Mine_Css.h_v}>
        <Text
          style={{...Mine_Css.h_t, width: '10%'}}
          onPress={() => navigation.goBack()}>
          {'<'}
        </Text>
        <View style={Mine_Css.h_v_1}>
          <TouchableOpacity
            style={Mine_Css.h_to}
            onPress={() => {
              setCardW({
                height: '48%',
                width: '48%',
                title: fakeData,
                margin: '1%',
              });
            }}>
            <Text style={Mine_Css.h_t}>2 x 2</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Mine_Css.h_to}
            onPress={() => {
              setCardW({
                height: '20%',
                width: '20%',
                title: fakeData1,
                margin: '2%',
              });
            }}>
            <Text style={Mine_Css.h_t}>4 x 4</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Mine_Css.h_to}
            onPress={() => {
              setCardW({
                height: '10%',
                width: '10%',
                title: fakeData2,
                margin: '0.7%',
              });
            }}>
            <Text style={Mine_Css.h_t}>8 x 8</Text>
          </TouchableOpacity>
        </View>
        <Text style={Mine_Css.h_t} onPress={() => setModalRule(true)}>
          Rule
        </Text>
      </View>
      {/* body */}
      <View style={Mine_Css.b_v}>
        {cardW.title.map((data, index) => {
          return (
            <TouchableOpacity
              key={index}
              style={{
                width: cardW.width,
                height: cardW.height,
                backgroundColor: 'rgba(0,0,0,0.3)',
                margin: cardW.margin,
                borderRadius: 10,
              }}></TouchableOpacity>
          );
        })}
        <TouchableOpacity style={Mine_Css.c_To} onPress={() => setModal(true)}>
          <Text style={Mine_Css.c_Te}>Start</Text>
        </TouchableOpacity>
      </View>
      {/* message */}
      <ScrollView style={{height: '50%'}}>
        <View
          style={{
            backgroundColor: '#feefdc',
            width: '90%',
            alignSelf: 'center',
            marginTop: '5%',
            borderRadius: 15,
          }}>
          <Text
            style={{
              color: '#000000',
              padding: '5%',
              fontWeight: '700',
              fontSize: 17,
            }}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora
            suscipit voluptatum voluptates quos ullam dignissimos et mollitia
            soluta, praesentium debitis.
          </Text>
        </View>
        {/* chart */}
        <Chart tableData={tableData} tableHead={tableHead} type="MINE" />
      </ScrollView>
    </View>
  );
};
export default MineSweeper;
