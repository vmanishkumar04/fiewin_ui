import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import React from 'react';
import Fast_Css from '../../../../Utilities/Css/Fast_css';
import Mine_css from '../../../../Utilities/Css/Mine_Css';
import {
  MINE_1,
  MINE_2,
  MINE_3,
  MINE_4,
  MINE_5,
  MINE_6,
} from '../../../../Constants/Images.constant';
const data =
  ' Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora suscipitvoluptatum voluptates quos ullam dignissimos et mollitia soluta, praesentiumdebitis.';

const Mine_Rule = ({modal, setModal}) => {
  return (
    <Modal transparent={true} visible={modal}>
      <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.3)'}}>
        <View style={Fast_Css.modal_v}>
          <ScrollView>
            <Text
              style={{
                ...Fast_Css.parity,
                backgroundColor: '#ffc538',
                marginTop: '3%',
                borderRadius: 30,
                color: '#ffffff',
              }}>
              Mine Rule
            </Text>
            {/* text 1 */}
            <Text style={Mine_css.rule_T}>{data}</Text>
            {/* Imag 1 */}
            <Image
              style={Mine_css.rule_Img}
              source={MINE_1}
              resizeMode="contain"
            />
            {/* text 2 */}
            <Text style={Mine_css.rule_T}>{data}</Text>
            {/* Image 2 */}
            <Image
              style={Mine_css.rule_Img}
              source={MINE_2}
              resizeMode="contain"
            />
            {/* text 3 */}
            <Text style={Mine_css.rule_T}>{data}</Text>
            {/*  Image 3 */}
            <Image
              style={Mine_css.rule_Img}
              source={MINE_3}
              resizeMode="contain"
            />
            <Image
              style={Mine_css.rule_Img}
              source={MINE_4}
              resizeMode="contain"
            />
            <Image
              style={Mine_css.rule_Img}
              source={MINE_5}
              resizeMode="contain"
            />
            {/* text 4 */}
            <Text style={Mine_css.rule_T}>{data}</Text>
            {/*  Image 4 */}
            <Image
              style={Mine_css.rule_Img}
              source={MINE_6}
              resizeMode="contain"
            />
            {/* over */}
          </ScrollView>
          {/* confirm btn */}
          <TouchableOpacity
            style={Fast_Css.confirm_Btn}
            onPress={() => setModal(false)}>
            <Text style={{color: '#ffffff', fontWeight: '700', fontSize: 17}}>
              Got it
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default Mine_Rule;
