import {View, Text, Modal, TouchableOpacity, ScrollView} from 'react-native';
import React, {useState} from 'react';
import Fast_Css from '../../../../Utilities/Css/Fast_css';
import Mine_css from '../../../../Utilities/Css/Mine_Css';
const Mine_Modal = ({modal, setModal, nav}) => {
  const [total, setTotal] = useState(0);
  return (
    <Modal transparent={true} visible={modal}>
      <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.3)'}}>
        <ScrollView style={Fast_Css.modal_v}>
          {/* head */}
          <Text style={Fast_Css.parity}>Choose Game Amount</Text>
          {/* head btn */}
          <View style={Fast_Css.point}>
            <View style={Fast_Css.point1}>
              <Text style={{color: '#000000', fontSize: 15, fontWeight: '500'}}>
                Point
              </Text>
              <Text style={{color: '#000000', fontSize: 20, fontWeight: '800'}}>
                50.00
              </Text>
            </View>
            <TouchableOpacity style={Fast_Css.recharge_btn} onPress={nav}>
              <Text style={{color: '#ffffff'}}>Recharge</Text>
            </TouchableOpacity>
          </View>
          {/* number point */}
          {/* need to run map */}
          <View style={Fast_Css.same_V}>
            <TouchableOpacity
              style={{
                ...Fast_Css.c_point,
                backgroundColor: 'rgba(230,230,230,0.8)',
                borderRadius: 50,
                paddingVertical: '7%',
              }}>
              <Text style={{color: '#000000', fontWeight: '500', fontSize: 17}}>
                -5
              </Text>
            </TouchableOpacity>
          </View>
          {/* add */}
          <View style={Mine_css.a_b}>
            <TouchableOpacity
              style={Mine_css.ab_touch}
              onPress={() => setTotal(total + 10)}>
              <Text style={Mine_css.a_b_t}>+</Text>
            </TouchableOpacity>
            <Text style={Mine_css.a_b_t}>{total}</Text>
            <TouchableOpacity
              style={Mine_css.ab_touch}
              onPress={() =>
                total >= 10 ? setTotal(total - 10) : setTotal(0)
              }>
              <Text style={Mine_css.a_b_t}>-</Text>
            </TouchableOpacity>
          </View>
          {/* confirm btn */}
          <TouchableOpacity
            style={Fast_Css.confirm_Btn}
            onPress={() => setModal(false)}>
            <Text style={{color: '#ffffff', fontWeight: '700', fontSize: 17}}>
              Confirm
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </Modal>
  );
};

export default Mine_Modal;
