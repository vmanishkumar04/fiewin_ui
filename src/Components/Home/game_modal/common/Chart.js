import {View, Text, TouchableOpacity, Image} from 'react-native';
import React, {useState} from 'react';
import Fast_Css from '../../../../Utilities/Css/Fast_css';
import {PR_LOGO} from '../../../../Constants/Images.constant';
const Chart = ({tableData, tableHead, type}) => {
  const [color1, setColor1] = useState(true);

  return (
    <View style={{marginVertical: '10%'}}>
      <View style={Fast_Css.sec5_V}>
        <TouchableOpacity
          style={{
            ...Fast_Css.sec5_V_1,
            borderBottomColor: color1 ? 'rgb(0,147,255)' : 'transparent',
          }}
          onPress={() => {
            setColor1(!color1);
          }}>
          <Text style={Fast_Css.order}>Everyone's Order</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            ...Fast_Css.sec5_V_1,
            borderBottomColor: !color1 ? 'rgb(0,147,255)' : 'transparent',
          }}
          onPress={() => {
            setColor1(!color1);
          }}>
          <Text style={Fast_Css.order}>My Order</Text>
        </TouchableOpacity>
      </View>
      {/* chart start */}
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          paddingVertical: '4%',
        }}>
        {tableHead.map((data, index) => {
          return (
            <Text style={{...Fast_Css.table, width: data.width}} key={index}>
              {data.data}
            </Text>
          );
        })}
      </View>
      {/* users */}
      {tableData.map((data, index) => {
        return (
          <View
            key={index}
            style={{
              flexDirection: 'row',
              paddingVertical: '2%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={Fast_Css.table_text}>{data.n1}</Text>
            {type == 'FAST' && (
              <View
                style={{
                  width: data.width,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image source={PR_LOGO} style={{height: 30, width: 30}} />
                <Text style={Fast_Css.userS}>***{data.n3}</Text>
                <Text
                  style={{
                    ...Fast_Css.userS,
                    backgroundColor: data.color,
                    padding: '5%',
                    textAlign: 'center',
                    borderRadius: 50,
                    paddingHorizontal: '6%',
                  }}>
                  {data.n4}
                </Text>
              </View>
            )}
            {type == 'MINE' && (
              <View
                style={{
                  width: '45%',
                  flexDirection: 'row',
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                <Image source={PR_LOGO} style={{height: 30, width: 30}} />
                <Text style={Fast_Css.userS}>***{data.n3}</Text>
                <Text
                  style={{
                    ...Fast_Css.userS,
                  }}>
                  {data.n5}
                </Text>
                <Text
                  style={{
                    ...Fast_Css.userS,
                    textAlign: 'center',
                  }}>
                  {data.n4}
                </Text>
              </View>
            )}
            <Text style={Fast_Css.table_text}>${data.n2}</Text>
          </View>
        );
      })}
    </View>
  );
};

export default Chart;
