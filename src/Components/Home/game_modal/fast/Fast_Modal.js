import {View, Text, Modal, TouchableOpacity, ScrollView} from 'react-native';
import React, {useState, useEffect} from 'react';
import Fast_Css from '../../../../Utilities/Css/Fast_css';
const Fast_Modal = ({send, modal, setModal, nav}) => {
  const contract = [{data: 10}, {data: 100}, {data: 1000}, {data: 10000}];
  const number = [{data: -5}, {data: -1}, {data: +1}, {data: +5}];
  useEffect(() => {
    if (bid < 1) {
      setBid(1);
    }
  });
  const [bid, setBid] = useState(1);
  const [delivery, setDelivery] = useState(1);
  const [fee, setFee] = useState(0.02);
  const [contractBid, setContract] = useState(10);
  const [amount, setAmount] = useState();
  const [numberR, setNumberR] = useState(2);
  return (
    <Modal transparent={true} visible={modal}>
      <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.3)'}}>
        <View style={Fast_Css.modal_v}>
          {/* head */}
          <Text style={{...Fast_Css.parity, color: send?.color || '#000000'}}>
            Parity {send?.text}
          </Text>
          {/* head btn */}
          <View style={Fast_Css.point}>
            <View style={Fast_Css.point1}>
              <Text style={{color: '#000000', fontSize: 15, fontWeight: '500'}}>
                Point
              </Text>
              <Text style={{color: '#000000', fontSize: 20, fontWeight: '800'}}>
                50.00
              </Text>
            </View>
            <TouchableOpacity style={Fast_Css.recharge_btn} onPress={nav}>
              <Text style={{color: '#ffffff'}}>Recharge</Text>
            </TouchableOpacity>
          </View>
          {/* text */}
          <Text style={Fast_Css.contract}>Contract Point</Text>
          {/* contract point */}
          <View style={Fast_Css.same_V}>
            {contract.map((data, index) => {
              return (
                <TouchableOpacity
                  key={index}
                  style={{
                    ...Fast_Css.c_point,
                    padding: '4%',
                    paddingVertical: '5%',
                  }}
                  onPress={() => setContract(data.data)}>
                  <Text style={{color: '#ffffff', fontWeight: '500'}}>
                    {data.data}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
          {/* text */}
          <Text style={Fast_Css.contract}>Number</Text>
          {/* number point */}
          <View style={Fast_Css.same_V}>
            {number.map((data, index) => {
              return (
                <TouchableOpacity
                  style={{
                    ...Fast_Css.c_point,
                    backgroundColor: 'rgba(230,230,230,0.8)',
                  }}
                  key={index}
                  onPress={() => {
                    setBid(bid + data.data);
                  }}>
                  <Text
                    style={{
                      color: '#000000',
                      fontWeight: '500',
                      fontSize: 17,
                    }}>
                    {data.data}
                  </Text>
                </TouchableOpacity>
              );
            })}
          </View>
          <Text
            style={{
              color: 'red',
              alignSelf: 'center',
              fontWeight: '700',
              fontSize: 18,
            }}>
            {bid}
          </Text>
          {/* results */}
          <View style={Fast_Css.result}>
            <View style={Fast_Css.result1}>
              <Text style={{color: 'rgba(0,0,0,0.5)', fontWeight: '500'}}>
                Delivery :
              </Text>
              <Text style={{fontSize: 16, fontWeight: '600', color: '#000000'}}>
                {(delivery * contractBid - fee * contractBid * bid).toFixed(2)}
              </Text>
            </View>
            <View style={Fast_Css.result1}>
              <Text style={{color: 'rgba(0,0,0,0.5)', fontWeight: '500'}}>
                Fee :
              </Text>
              <Text style={{fontSize: 16, fontWeight: '600', color: '#000000'}}>
                {(fee * contractBid * bid).toFixed(2)}
              </Text>
            </View>
            <View style={Fast_Css.result1}>
              <Text style={{color: 'rgba(0,0,0,0.5)', fontWeight: '500'}}>
                Amount :
              </Text>
              <Text style={{fontSize: 16, fontWeight: '600', color: '#000000'}}>
                {(delivery * contractBid - fee * contractBid * bid).toFixed(2) *
                  numberR}
              </Text>
            </View>
          </View>
          {/* confirm btn */}
          <TouchableOpacity
            style={Fast_Css.confirm_Btn}
            onPress={() => setModal(false)}>
            <Text style={{color: '#ffffff', fontWeight: '700', fontSize: 17}}>
              Confirm
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default Fast_Modal;
