import {View, Text, Modal, TouchableOpacity, ScrollView} from 'react-native';
import React from 'react';
import Fast_Css from '../../../../Utilities/Css/Fast_css';
import Table from './Table';
const Fast_Rule = ({send, modal, setModal}) => {
  const Rule_Data = [
    {
      sn: 1,
      data: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia placeat velitmagni voluptates tenetur? Atque voluptas beatae consequuntur debitislaudantium reiciendis quidem iure molestias molestiae quas corporis, fugiat,totam delectus vel earum voluptate nam laboriosam unde nostrum. Explicabo,numquam cumque!',
    },
    {
      sn: 2,
      data: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia placeat velitmagni voluptates tenetur? Atque voluptas beatae consequuntur debitislaudantium reiciendis quidem iure molestias molestiae quas corporis, fugiat,totam delectus vel earum voluptate nam laboriosam unde nostrum. Explicabo,numquam cumque!',
    },
    {
      sn: 3,
      data: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia placeat velitmagni voluptates tenetur? Atque voluptas beatae consequuntur debitislaudantium reiciendis quidem iure molestias molestiae quas corporis, fugiat,totam delectus vel earum voluptate nam laboriosam unde nostrum. Explicabo,numquam cumque!',
    },
  ];
  const bold_Data =
    'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia placeat velitmagni voluptates tenetur? Atque voluptas beatae consequuntur debitislaudantium reiciendis quidem iure molestias molestiae quas corporis, fugiat,totam delectus vel earum voluptate nam laboriosam unde nostrum. Explicabo,numquam cumque!';
  return (
    <Modal transparent={true} visible={modal}>
      <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.3)'}}>
        <ScrollView style={Fast_Css.modal_v}>
          {/* head */}
          <Text style={{...Fast_Css.r_h_t}}>{send?.text} Rule</Text>
          {/* head Rule */}
          <Text style={Fast_Css.h_rule}>{bold_Data}</Text>
          {/* rule list */}
          {Rule_Data.map((data, index) => {
            return (
              <View
                style={{
                  flexDirection: 'row',
                  width: '90%',
                  alignSelf: 'center',
                  marginBottom: '3%',
                  alignItems: 'center',
                }}
                key={index}>
                <Text style={Fast_Css.list_c}>{data.sn}</Text>
                <Text
                  style={{
                    color: '#000000',
                    textAlign: 'justify',
                    width: '80%',
                  }}>
                  {data.data}
                </Text>
              </View>
            );
          })}

          {/* table */}
          <Table />
          {/* confirm btn */}
          <TouchableOpacity
            style={Fast_Css.confirm_Btn}
            onPress={() => setModal(false)}>
            <Text style={{color: '#ffffff', fontWeight: '700', fontSize: 17}}>
              Got it
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </Modal>
  );
};

export default Fast_Rule;
