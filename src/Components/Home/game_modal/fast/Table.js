import {View, Text} from 'react-native';
import React from 'react';
const Table = () => {
  const table_D = [
    {
      title: 'Join Green',
      color: 'green',
      row1: {
        title: '-----',
        width: '30%',
      },
      row2: {
        title: '1,2,3,4',
        title1: '3',
        width: '40%',
      },
      row3: {
        title: '2,2,3',
        title1: '1.5',
        width: '30%',
      },
    },
    {
      title: 'Join Red',
      color: 'red',
      row1: {
        title: '-----',
        width: '30%',
      },
      row2: {
        title: '1,2,3,4',
        title1: '3',
        width: '40%',
      },
      row3: {
        title: '2,2,3',
        title1: '1.5',
        width: '30%',
      },
    },
    {
      title: 'Join Voilet',
      color: 'purple',
      row1: {
        title: '-----',
        width: '30%',
      },
      row2: {
        title: '1,2,3,4',
        width: '40%',
      },
      row3: {
        title: '2,2,3',
        width: '30%',
      },
    },
  ];
  const table_H = [
    {title: 'Select', width: '30%'},
    {title: 'Result', width: '40%'},
    {title: 'Mulitplayer', width: '30%'},
  ];

  return (
    <View style={{width: '90%', alignSelf: 'center', marginTop: '2%'}}>
      <View
        style={{
          flexDirection: 'row',
        }}>
        {table_H.map((data, index) => {
          return (
            <Text
              key={index}
              style={{
                textAlign: 'center',
                color: '#000000',
                width: data.width,
                borderWidth: 1,
                padding: '2%',
                backgroundColor: 'rgba(240,230,230,0.8)',
                borderColor: '#cccccc',
              }}>
              {data.title}
            </Text>
          );
        })}
      </View>
      {/* table data */}
      <View>
        {table_D.map((data, index) => {
          return (
            <View key={index} style={{flexDirection: 'row'}}>
              <Text
                style={{
                  width: data.row1.width,
                  color: data.color,
                  textAlign: 'center',
                  padding: '4%',
                  borderWidth: 1,
                  borderColor: '#cccccc',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {data.title}
              </Text>
              <View
                style={{
                  borderWidth: 1,
                  width: data.row2.width,
                  borderColor: '#cccccc',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: data.color,
                    textAlign: 'center',
                    padding: '4%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {data.row2.title}
                </Text>
                {data?.row2?.title1?.length > 0 && (
                  <Text
                    style={{
                      color: data.color,
                      textAlign: 'center',
                      padding: '4%',
                      borderTopWidth: 1,
                      borderColor: '#cccccc',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {data?.row2?.title1}
                  </Text>
                )}
              </View>
              <View
                style={{
                  borderWidth: 1,
                  width: data.row3.width,
                  borderColor: '#cccccc',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    color: data.color,
                    textAlign: 'center',
                    padding: '4%',
                    justifyContent: 'center',
                  }}>
                  {data.row3.title}
                </Text>
                {data?.row3?.title1?.length > 0 && (
                  <Text
                    style={{
                      color: data.color,
                      textAlign: 'center',
                      padding: '4%',
                      borderTopWidth: 1,
                      borderColor: '#cccccc',
                      justifyContent: 'center',
                    }}>
                    {data?.row3?.title1}
                  </Text>
                )}
              </View>
            </View>
          );
        })}
      </View>
    </View>
  );
};

export default Table;
