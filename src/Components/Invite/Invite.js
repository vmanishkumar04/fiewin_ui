import {Image, Text, TouchableOpacity, View, ScrollView} from 'react-native';
import React from 'react';
import Invite_Css from '../../Utilities/Css/Invite_Css';
import {
  INVITE_BACK,
  INVITE_BANNER,
  INVITE_PRE,
  INVITE_RANK,
  INVITE_MY,
  INVITE_BTN,
} from '../../Constants/Images.constant';
const Invite = () => {
  return (
    <ScrollView>
      <View
        style={{
          minHeight: '20%',
          position: 'relative',
          backgroundColor: '#cbcbcb1f',
        }}>
        {/* header Image */}
        <Image
          source={INVITE_BACK}
          style={Invite_Css.BackImg}
          resizeMethod="scale"
        />
        {/* header Card */}
        <View style={Invite_Css.headerCard}>
          <View>
            <Text style={Invite_Css.textColor}>Agent Amount</Text>
            <Text style={Invite_Css.textColor}>100 Rs</Text>
          </View>
          <TouchableOpacity style={Invite_Css.headerCardBtn}>
            <Text style={Invite_Css.headerCardBtnText}>Withdraw</Text>
          </TouchableOpacity>
        </View>
        {/* header bottom card */}
        <View style={Invite_Css.B_Card}>
          <TouchableOpacity style={Invite_Css.b_Imgcard}>
            <Image
              source={INVITE_PRE}
              style={{width: '100%', height: '100%'}}
            />
          </TouchableOpacity>
          <TouchableOpacity style={Invite_Css.b_Imgcard}>
            <Image
              source={INVITE_RANK}
              style={{width: '100%', height: '100%'}}
            />
          </TouchableOpacity>
          <TouchableOpacity style={Invite_Css.b_Imgcard}>
            <Image source={INVITE_MY} style={{width: '100%', height: '100%'}} />
          </TouchableOpacity>
        </View>
        {/* body content */}
        <View style={{backgroundColor: '#ffffff', zIndex: -1}}>
          <View
            style={{
              marginTop: '18%',
              backgroundColor: '#f5f5f5',
              maxWidth: '100%',
              marginHorizontal: '3%',
            }}>
            <Image
              source={INVITE_BANNER}
              style={Invite_Css.bodyImg}
              resizeMode="cover"
            />
          </View>
          {/* income & Invite */}
          <View
            style={{
              ...Invite_Css.II_container,
              backgroundColor: '#f5f5f5',
              minHeight: '15%',
            }}>
            <View style={{width: '50%'}}>
              <Text style={Invite_Css.II_TextCenter}>Invited Today</Text>
              <Text
                style={{
                  ...Invite_Css.II_TextCenter,
                  fontWeight: '500',
                  fontSize: 17,
                }}>
                &#8377; 0
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Text style={{...Invite_Css.II_TextCenter, marginRight: '5%'}}>
                  Total 0
                </Text>
                <Image source={INVITE_BTN} style={{height: 20, width: 22}} />
              </View>
            </View>
            <View style={{width: '50%'}}>
              <Text style={Invite_Css.II_TextCenter}>Invited Today</Text>
              <Text
                style={{
                  ...Invite_Css.II_TextCenter,
                  fontWeight: '500',
                  fontSize: 17,
                }}>
                &#8377; 0
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  width: '100%',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    ...Invite_Css.II_TextCenter,
                    marginRight: '5%',
                  }}>
                  Total 0
                </Text>
                <Image
                  source={INVITE_BTN}
                  style={{
                    height: 20,
                    width: 22,
                  }}
                />
              </View>
            </View>
          </View>
        </View>
        {/* footer */}
        <View style={Invite_Css.footerConatainer}>
          <View style={Invite_Css.incomeText}>
            <Text style={{fontSize: 19}}>Income Details</Text>
            <Text style={{fontSize: 19}}>More {'>'}</Text>
          </View>
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 19}}>No Income</Text>
            <TouchableOpacity style={Invite_Css.inviteBtn}>
              <Text style={{fontSize: 16}}>Invite Now</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Invite;
