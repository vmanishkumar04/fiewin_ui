import {Image, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {
  Dummy,
  ABOUT_B,
  ABOUT_HOT,
  ABOUT_6_1,
} from '../../../Constants/Images.constant';
import About_card from '../../../Assets/Card/About_card';
import About_Css from '../../../Utilities/Css/About_css';
import {
  headerData,
  bodyCard_Hottest,
  restBodyData,
  footerData,
} from '../../../Assets/data/About_Data';
const About = () => {
  return (
    <>
      <ScrollView style={{height: '100%', borderWidth: 1}}>
        {/* headder */}
        <View
          style={{
            height: 500,
          }}>
          <Image source={ABOUT_B} style={About_Css.header} />
          <View style={About_Css.headerView}>
            {headerData.map((data, index) => {
              return (
                <View key={index} style={About_Css.headerInnerView}>
                  <Image
                    source={data.img}
                    style={{height: 30, width: 30, borderRadius: 30}}
                  />
                  <Text style={About_Css.headerInnerViewText}>
                    {data.title1}
                  </Text>
                  <Text
                    style={{
                      ...About_Css.headerInnerViewText,
                      fontWeight: '700',
                      fontSize: 20,
                    }}>
                    {data.title2}
                  </Text>
                </View>
              );
            })}
          </View>
        </View>
        {/* // sesion 2 body */}
        {/* card session */}
        <View style={About_Css.hottest_product}>
          <Image
            source={ABOUT_HOT}
            style={{height: 50, width: 50, borderRadius: 30}}
          />
          <Text
            style={{
              ...About_Css.hottest_productText,
              fontWeight: '900',
              fontSize: 22,
              color: '#000000',
            }}>
            The Hottest Product
          </Text>
          <Text style={About_Css.hottest_productText}>
            Innovation product model
          </Text>
          <Text style={About_Css.hottest_productText}>
            The best gaming experience
          </Text>
        </View>
        <View style={About_Css.hottest_map_view}>
          {bodyCard_Hottest.map((data, index) => {
            return (
              <TouchableOpacity key={index} style={About_Css.hottest_Tochable}>
                <Image
                  source={data.img}
                  style={{
                    height: '100%',
                    width: '100%',
                    position: 'absolute',
                  }}
                />
                {/* <Text style={About_Css.hottest_Tochable_Text1}>
                  {data.title1}
                </Text> */}
                <Text style={About_Css.hottest_Tochable_Text2}>
                  {data.title2}
                </Text>
                <Text style={About_Css.hottest_Tochable_Text3}>
                  {data.title3}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
        {/* card */}
        <View>
          {restBodyData.map((data, index) => {
            return <About_card data={data} key={index} />;
          })}
        </View>
        {/* global services */}
        <View style={About_Css.globalService}>
          <Text style={About_Css.globalServiceText}>Global service</Text>
          <Image source={ABOUT_6_1} style={{width: '100%', height: 300}} />
        </View>
        {/* footer */}
        <View style={About_Css.footerView}>
          <View style={About_Css.footterMapContainer}>
            {footerData.map((data, index) => {
              return (
                <View key={index} style={About_Css.footterMapContainer_View}>
                  <Image
                    source={data.source}
                    style={{height: 50, width: 50, borderRadius: 30}}
                  />
                  <Text style={About_Css.footterMapContainer_Text}>
                    {data.data}
                  </Text>
                </View>
              );
            })}
          </View>
          <Text style={About_Css.footerText1}>
            Contact us if you have any question
          </Text>
          <Text style={About_Css.footerText2}>support@fiewin.in</Text>
          <Text style={About_Css.footerText3}>
            @2021-2023 Fiewin.com. All rights reserved
          </Text>
        </View>
      </ScrollView>
    </>
  );
};

export default About;
