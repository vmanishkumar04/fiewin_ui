import {
  Image,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
  Modal,
} from 'react-native';
import React, {useState} from 'react';
import Profile_Css from '../../Utilities/Css/Profile_css';
import {
  PR_LOGO,
  PR_ORDER,
  PR_SUPPORT,
  PR_FINANCE,
  PR_FOLLOW,
  PR_ABOUT,
} from '../../Constants/Images.constant';
import Upload from '../upload/Upload';
const Profile = ({navigation}) => {
  const bodyData = [
    {data: 'Order Record', img: PR_ORDER},
    {data: 'Financial Details', img: PR_FINANCE},
  ];
  const bodyData2 = [
    {data: 'About us', img: PR_ABOUT},
    {data: 'Support', img: PR_SUPPORT},
    {data: 'Follow us', img: PR_FOLLOW},
    {data: 'Version 1.0(latest)'},
  ];
  const findClick = data => {
    if (data == 'About us') {
      return navigation.navigate('About_Us');
    }
  };
  const [modal, setModal] = useState(false);
  return (
    <View style={{flex: 1, backgroundColor: '#f5f5f5'}}>
      <Upload modal={modal} setModal={setModal} />
      {/* div 1 */}
      <View style={Profile_Css.headContainer}>
        {/* heading */}
        <Text style={Profile_Css.headText}>Vashudev</Text>
        {/* detail */}
        <View style={Profile_Css.container}>
          <TouchableOpacity
            style={Profile_Css.headImgContainer}
            onPress={() => setModal(!modal)}>
            <Image source={PR_LOGO} style={Profile_Css.headImg} />
            <View>
              <Text style={{...Profile_Css.detailText, color: '#000000'}}>
                ID: {'123456'}
              </Text>
              <Text style={Profile_Css.detailText}>Phone: {'8736079780'}</Text>
              <Text style={Profile_Css.detailText}>NickName: {'vashu'}</Text>
            </View>
          </TouchableOpacity>
          <Text style={Profile_Css.ModifyText}>Modify {'>'}</Text>
        </View>
      </View>
      {/* div 2 */}
      <ScrollView>
        <View style={Profile_Css.bodyMainContainer}>
          {bodyData.map((data, index) => {
            return (
              <TouchableOpacity key={index} style={Profile_Css.bodyContainer}>
                <View style={Profile_Css.bodyImgContainer}>
                  <Image
                    source={data.img}
                    style={Profile_Css.bodyImg}
                    resizeMode="cover"
                  />
                  {/* </View> */}
                  <Text style={Profile_Css.div2Text}>{data.data}</Text>
                </View>
                <Text style={Profile_Css.arrowFont}>{'>'}</Text>
              </TouchableOpacity>
            );
          })}
        </View>
        {/* div 3 */}
        <View style={Profile_Css.div3MainContainer}>
          {bodyData2.map((data, index) => {
            if (data.data.slice(0, 7) == 'Version') {
              return (
                <TouchableOpacity
                  key={index}
                  style={Profile_Css.bodyContainer}
                  onPress={() => findClick(data?.data)}>
                  <View style={Profile_Css.bodyImgContainer}>
                    <Image source={data.img} style={Profile_Css.bodyImg} />
                    <Text style={Profile_Css.div2Text}>{data.data}</Text>
                  </View>
                </TouchableOpacity>
              );
            } else {
              return (
                <TouchableOpacity
                  key={index}
                  style={Profile_Css.bodyContainer}
                  onPress={() => findClick(data?.data)}>
                  <View style={Profile_Css.bodyImgContainer}>
                    <Image source={data.img} style={Profile_Css.bodyImg} />
                    <Text style={Profile_Css.div2Text}>{data.data}</Text>
                  </View>
                  <Text style={Profile_Css.arrowFont}>{'>'}</Text>
                </TouchableOpacity>
              );
            }
          })}
        </View>
        <Text
          style={Profile_Css.signOutText}
          onPress={() => navigation.navigate('Login')}>
          Sign Out
        </Text>
      </ScrollView>
    </View>
  );
};

export default Profile;
