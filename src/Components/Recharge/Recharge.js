import {Text, TextInput, TouchableOpacity, View, Image} from 'react-native';
import React from 'react';
import Recharge_Css from '../../Utilities/Css/Recharge_css';
import {Dummy, PR_LOGO, RE_S, RE_F} from '../../Constants/Images.constant';
const Recharge = () => {
  const price = [
    {amt: 35},
    {amt: 250},
    {amt: 1500},
    {amt: 2500},
    {amt: 3500},
    {amt: 35000},
  ];
  return (
    <View>
      {/* head  */}
      <View style={Recharge_Css.headContainer}>
        <Text style={Recharge_Css.headText}>Records</Text>
        <Text
          style={{
            ...Recharge_Css.headText,
            fontSize: 20,
            fontWeight: '700',
            color: '#000000',
          }}>
          Recharge
        </Text>
        <Text style={Recharge_Css.headText}>Help</Text>
      </View>
      {/* body */}
      <View>
        <View style={Recharge_Css.bodyBalance}>
          <Text style={Recharge_Css.bodyBalText}>Balance</Text>
          <Text
            style={{
              ...Recharge_Css.bodyBalText,
              fontWeight: '800',
              fontSize: 25,
              color: '#000000',
            }}>
            &#8377; 0.000
          </Text>
        </View>
        {/* input */}
        <View style={Recharge_Css.inpMainContainer}>
          <Text style={Recharge_Css.inpAmtText}>Amount</Text>
          <View style={Recharge_Css.inpContainer}>
            <Text style={Recharge_Css.inpAmtText}>&#8377;</Text>
            <TextInput
              style={Recharge_Css.inpBox}
              placeholder="29 ~ 50000"
              placeholderTextColor="#7a7474f5"
            />
          </View>
        </View>
        {/* card */}
        <View style={Recharge_Css.cardContainer}>
          {price.map((data, index) => {
            return (
              <TouchableOpacity key={index} style={Recharge_Css.cardText}>
                <Text style={Recharge_Css.amtText}>{data.amt}</Text>
              </TouchableOpacity>
            );
          })}
        </View>
        {/* recharge btn  */}
        <TouchableOpacity style={Recharge_Css.btn}>
          <Text style={Recharge_Css.btnText}>Recharge</Text>
        </TouchableOpacity>
        {/* icons */}
        <View style={Recharge_Css.icon}>
          <View style={Recharge_Css.iconImgContainer}>
            <Image source={RE_S} style={Recharge_Css.iconSize} />
            <Text style={Recharge_Css.iconText}>Security</Text>
          </View>
          <View style={Recharge_Css.iconImgContainer}>
            <Image source={RE_F} style={Recharge_Css.iconSize} />
            <Text style={Recharge_Css.iconText}>Fast</Text>
          </View>
        </View>
      </View>
      {/* footer */}
      <View style={Recharge_Css.footerContainer}>
        <Image source={PR_LOGO} style={Recharge_Css.iconSize} />
        <Text style={Recharge_Css.footerText}>
          {'****652'} Sucessfully recharge $ {'100'}
        </Text>
      </View>
    </View>
  );
};
export default Recharge;
