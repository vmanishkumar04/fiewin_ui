import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Signup_css from '../../Utilities/Css/SignUp_Css';
import {postFetch} from '../../api/api';
import {getDeviceId} from 'react-native-device-info';
import {OTP, SIGNUP} from '../../Constants/ApiConstant';
import LoginCard from './Login/LoginCard';
import {LoginBackGround} from '../../Constants/Images.constant';
import AsyncStorage from '@react-native-async-storage/async-storage';
const Login = ({navigation}) => {
  useEffect(() => {
    let getDeviceData = async () => {
      let id = await getDeviceId();
      setData({...data, device_Id: id});
    };
    getDeviceData();
    return () => {
      console.log('Clean Up ');
    };
  }, [data]);
  const [data, setData] = useState({
    name: '',
    mobileNo: '',
    emailid: '',
    OTP: '',
    device_Id: '',
  });
  const [loadPage, setLoadPage] = useState('login');
  const [btn, setBtn] = useState('Get OTP');
  const nav = () => {
    return navigation.replace('Root');
  };
  // validation
  const [validater, setValidater] = useState({});
  const [response, setResponse] = useState('');
  const [enableBtn, setEnableBtn] = useState(false);
  const emailRegex =
    /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
  const checkValidation = () => {
    let isValidate = true;
    const {name, mobileNo, emailid} = data;
    if (!name) {
      validater.name = 'Name is required';
      isValidate = false;
    }
    if (!emailRegex.test(emailid)) {
      validater.emailid = 'Email is invalid';
      isValidate = false;
    }
    if (!mobileNo) {
      validater.mobileNo = 'Mobile Number is required';
      isValidate = false;
    }
    if (!emailid) {
      validater.emailid = 'Email is required';
      isValidate = false;
    }
    if (mobileNo && mobileNo.trim().length < 10) {
      validater.mobileNo = 'Invalid Number length';
      isValidate = false;
    }
    setValidater({...validater});
    return isValidate;
  };

  const handleSubmitForOtp = async event => {
    let isValidate = checkValidation();
    if (isValidate) {
      setResponse('');
      try {
        setBtn('Loading...');
        const res = await postFetch(OTP, data);
        if (res.data?.message?.statuscode == 1) {
          setEnableBtn(true);
          return setBtn('Sign up');
        }
      } catch (error) {
        setResponse('Sign up failed');
        setBtn('Get OTP');
        return error.response;
      }
    }
  };
  const handleSubmitForSign = async event => {
    let isValidate = checkValidation();
    if (isValidate) {
      setResponse('');
      try {
        const res = await postFetch(SIGNUP, data);
        if (res.statuscode == 1) {
          await AsyncStorage.setItem('login', 'login');
          return navigation.replace('Home1');
        }
      } catch (error) {
        setResponse('Sign up failed');
        return error.response;
      }
    }
  };
  return (
    <ImageBackground
      source={LoginBackGround}
      style={{
        flex: 1,
        justifyContent: 'center',
      }}>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'rgba(66 ,133, 244,0.6)',
        }}>
        <View style={Signup_css.head}>
          <TouchableOpacity
            style={Signup_css.headT}
            onPress={() => setLoadPage('login')}>
            <Text style={Signup_css.headText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Signup_css.headT}
            onPress={() => setLoadPage('signup')}>
            <Text style={Signup_css.headText}>Sign up</Text>
          </TouchableOpacity>
        </View>
        {loadPage == 'login' && <LoginCard nav={nav} />}
        {loadPage == 'signup' && (
          <View style={Signup_css.InputBox}>
            <Text style={Signup_css.label}>Name</Text>
            <TextInput
              style={Signup_css.inp}
              onChangeText={dataa => {
                setData({...data, name: dataa});
                setValidater({...validater, name: null});
              }}
            />
            {validater.name && (
              <View>
                <Text style={{color: 'red', fontSize: 18, textAlign: 'center'}}>
                  {validater.name}
                </Text>
              </View>
            )}
            <Text style={Signup_css.label}>Mobile Number</Text>
            <TextInput
              style={Signup_css.inp}
              onChangeText={dataa => {
                setData({...data, mobileNo: dataa});
                setValidater({...validater, mobileNo: null});
              }}
              maxLength={10}
            />
            {validater.mobileNo && (
              <View>
                <Text style={{color: 'red', fontSize: 18, textAlign: 'center'}}>
                  {validater.mobileNo}
                </Text>
              </View>
            )}
            <Text style={Signup_css.label}>Email</Text>
            <TextInput
              style={Signup_css.inp}
              onChangeText={dataa => {
                setData({...data, emailid: dataa});
                setValidater({...validater, emailid: null});
              }}
            />
            {validater.emailid && (
              <View>
                <Text style={{color: 'red', fontSize: 18, textAlign: 'center'}}>
                  {validater.emailid}
                </Text>
              </View>
            )}
            {btn == 'Sign up' && (
              <>
                <Text style={Signup_css.label}>OTP</Text>
                <TextInput
                  style={Signup_css.inp}
                  editable={enableBtn}
                  onChangeText={dataa => {
                    setData({...data, OTP: dataa});
                    setValidater({...validater, OTP: null});
                  }}
                />
                {validater.OTP && (
                  <View>
                    <Text
                      style={{color: 'red', fontSize: 18, textAlign: 'center'}}>
                      {validater.OTP}
                    </Text>
                  </View>
                )}
              </>
            )}

            {btn == 'Get OTP' && (
              <TouchableOpacity
                style={Signup_css.btn}
                onPress={handleSubmitForOtp}>
                <Text style={Signup_css.color}>{btn}</Text>
              </TouchableOpacity>
            )}
            {btn == 'Sign up' && (
              <TouchableOpacity
                style={Signup_css.btn}
                onPress={handleSubmitForSign}>
                <Text style={Signup_css.color}>{btn}</Text>
              </TouchableOpacity>
            )}
          </View>
        )}
        {response.length > 3 && (
          <View>
            <Text style={{color: 'red', fontSize: 18, textAlign: 'center'}}>
              {response}
            </Text>
          </View>
        )}
      </View>
    </ImageBackground>
  );
};

export default Login;
