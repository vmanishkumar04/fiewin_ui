import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';
import Signup_css from '../../../Utilities/Css/SignUp_Css';
import {getDeviceId} from 'react-native-device-info';
import {postFetch} from '../../../api/api';
import {LOGIN, LOGIN_OTP} from '../../../Constants/ApiConstant';
import AsyncStorage from '@react-native-async-storage/async-storage';
const LoginCard = ({nav}) => {
  useEffect(() => {
    let getDeviceData = async () => {
      let id = await getDeviceId();
      setLoginData({...loginData, deviceId: id});
    };
    getDeviceData();
    return () => {
      console.log('Clean Up ');
    };
  }, [loginData]);
  const [validaterLogin, setValidaterLogin] = useState({});
  const [btn, setBtn] = useState(false);
  const [btnMessage, setBtnMessage] = useState({
    login: 'Login',
    otp: 'GET OTP',
  });
  const [loginData, setLoginData] = useState({
    mobileNo: '',
    otp: '',
    deviceId: '',
  });
  const [response, setResponse] = useState({
    mobileNo: '',
    otp: '',
  });
  const handleSubmitForOtp = async () => {
    try {
      setBtnMessage({...btnMessage, otp: 'Loading'});
      const res = await postFetch(LOGIN_OTP, loginData);
      if (res?.data?.message?.statuscode == 1) {
        setBtnMessage({...btnMessage, otp: 'GET OTP'});
        setBtn(true);
      }
    } catch (error) {
      return console.log('Server Issue');
    }
  };
  const handleSubmitForLogin = async () => {
    try {
      setBtnMessage({...btnMessage, login: 'Loading...'});
      const res = await postFetch(LOGIN, loginData);
      if (res?.data?.message?.statuscode == 1) {
        setBtnMessage({...btnMessage, login: 'Login'});
        await AsyncStorage.setItem('login', 'login');
        return nav();
      }
    } catch (error) {
      return console.log('Server Issue', error);
    }
  };
  return (
    <View style={Signup_css.InputBox}>
      <Text style={Signup_css.label}>Mobile Number</Text>
      <TextInput
        style={Signup_css.inp}
        onChangeText={dataa => {
          setResponse({mobileNo: '', otp: ''});
          setLoginData({...loginData, mobileNo: dataa});
          setValidaterLogin({...validaterLogin, mobileNo: null});
        }}
        keyboardType="number-pad"
      />
      {btn && (
        <>
          <Text style={Signup_css.label}>OTP</Text>
          <TextInput
            style={Signup_css.inp}
            onChangeText={dataa => {
              setResponse({mobileNo: '', otp: ''});
              setLoginData({...loginData, otp: dataa});
              setValidaterLogin({...validaterLogin, mobileNo: null});
            }}
            keyboardType="number-pad"
          />
        </>
      )}
      {response.mobileNo.trim().length > 3 && (
        <Text style={{color: 'red', fontWeight: '500'}}>
          Invalid mobileNo Number
        </Text>
      )}
      {response.otp.trim().length > 3 && btn && (
        <Text style={{color: 'red', fontWeight: '500'}}>
          Invalid OTP Number
        </Text>
      )}
      {btn && (
        <TouchableOpacity
          style={Signup_css.btn}
          onPress={() =>
            loginData.mobileNo.trim().length > 9 &&
            loginData.otp.trim().length > 3
              ? handleSubmitForLogin()
              : setResponse({...response, mobileNo: 'Invalid mobileNo Number'})
          }>
          <Text style={Signup_css.color}>{btnMessage.login}</Text>
        </TouchableOpacity>
      )}
      {!btn && (
        <TouchableOpacity
          style={Signup_css.btn}
          onPress={() =>
            loginData.mobileNo.length > 9
              ? handleSubmitForOtp()
              : setResponse({...response, otp: 'Invalid OTP'})
          }>
          <Text style={Signup_css.color}>{btnMessage.otp}</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default LoginCard;
