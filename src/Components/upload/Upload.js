import {View, Text, Modal} from 'react-native';
import React, {useState} from 'react';
import ImagePicker from 'react-native-image-crop-picker';
const Upload = ({modal, setModal}) => {
  const takePicture = async () => {
    try {
      const data = await ImagePicker.openCamera({
        width: 300,
        height: 400,
        cropping: true,
      });
      setImages(data.path);
    } catch (err) {
      console.log(err);
    }
  };
  const openDocs = async () => {
    try {
      let imageStore = [];
      const res = await ImagePicker.openPicker({
        multiple: false,
      });
      console.log(res);
      imageStore = res.map(data => data.path);
      setImages(imageStore);
    } catch (err) {
      console.log(err);
    }
  };
  const [images, setImages] = useState([]);
  const uploadImage = async () => {
    const formData = new FormData();
    formData.append('packing_slip_code', 'SLIPBARCODE48716');
    formData.append('image', {
      uri: images[0],
      name: 'upload',
      fileName: 'image',
      type: 'image/jpg',
    });
    const response = await postFetch(SCANUPLOADIMAGES, formData);
  };
  return (
    <Modal>
      <View>
        <Text
          onPress={openDocs}
          style={{color: '#000000', alignSelf: 'center'}}>
          upload
        </Text>
      </View>
    </Modal>
  );
};

export default Upload;
