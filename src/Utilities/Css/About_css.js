import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  header: {
    width: '100%',
    height: '100%',
  },
  headerView: {
    position: 'absolute',
    justifyContent: 'center',
    alignSelf: 'center',
    height: '100%',
  },
  headerInnerView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15%',
  },
  headerInnerViewText: {
    fontSize: 16,
    marginTop: '1%',
    color: '#f0ece4',
  },
  //   hottest
  hottest_product: {
    marginTop: '5%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  hottest_productText: {
    fontSize: 16,
    color: '#7a7474f5',
  },
  hottest_map_view: {
    marginTop: '10%',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  hottest_Tochable: {
    width: '28%',
    marginTop: '3%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    position: 'relative',
    overflow: 'hidden',
    height: 200,
  },
  hottest_Tochable_Text1: {
    padding: '3%',
    marginTop: '5%',
    width: '50%',
    textAlign: 'center',
    borderRadius: 10,
    backgroundColor: '#3599abe6',
    color: '#ffffff',
    fontSize: 15,
    top: '18%',
  },
  hottest_Tochable_Text2: {
    color: '#ffffff',
    fontSize: 14,
    fontWeight: '500',
    paddingHorizontal: '5%',
    textAlign: 'center',
    top: '18%',
  },
  hottest_Tochable_Text3: {
    color: '#ffffff',
    fontSize: 13,
    paddingHorizontal: '5%',
    textAlign: 'center',
    top: '18%',
  },
  //    card
  cardContainer: {
    marginTop: '5%',
  },
  cardView: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  cardText1: {
    color: '#000000',
    fontWeight: '900',
    fontSize: 20,
    paddingHorizontal: '5%',
  },
  cardText2: {
    marginTop: '2%',
    fontSize: 16,
    color: '#7a7474f5',
    textAlign: 'center',
    paddingHorizontal: '5%',
  },
  imgData_map: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  imgDataInner: {
    width: '28%',
    alignItems: 'center',
    marginTop: '5%',
    maxHeight: '50%',
  },
  imgDataInnerText: {
    marginTop: '5%',
    color: '#7a7474f5',
    fontSize: 17,
    textAlign: 'center',
    overflow: 'hidden',
  },
  //   global service
  globalServiceText: {
    fontSize: 24,
    fontWeight: '900',
    color: '#000000',
    textAlign: 'center',
    marginVertical: '5%',
  },
  //   footer
  footerView: {
    backgroundColor: '#07073d',
    minHeight: 170,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: '8%',
  },
  footterMapContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginVertical: '5%',
  },
  footterMapContainer_View: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '45%',
  },
  footterMapContainer_Text: {
    marginTop: '3%',
    textAlign: 'center',
  },
  footerText1: {
    paddingHorizontal: '2%',
    fontSize: 20,
    fontWeight: '700',
  },
  footerText2: {
    color: '#02306f',
  },
  footerText3: {
    marginTop: '3%',
    color: '#ffffff',
  },
});
export default styles;
