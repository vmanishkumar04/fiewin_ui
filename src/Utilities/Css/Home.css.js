import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  news: {
    borderBottomWidth: 0.5,
    paddingVertical: '1.5%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  newsText: {
    fontWeight: '400',
    fontSize: 17,
    color: '#7a7474f5',
  },
  recharge: {
    flexDirection: 'row',
    marginVertical: '5%',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '5%',
    minHeight: '15%',
  },
  rechargeTextView: {
    width: '30%',
  },
  rechargeText: {
    fontWeight: '500',
    fontSize: 20,
    color: 'black',
  },
  btnView: {
    width: '70%',
    alignItems: 'flex-end',
  },
  btnTextTouch: {
    marginVertical: 10,
    borderRadius: 20,
    width: '60%',
    alignItems: 'center',
    backgroundColor: '#0092ffde',
  },
  btnText: {
    padding: '7%',
    fontWeight: '800',
  },
  taskContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderBottomWidth: 0.5,
    borderBottomColor: '#cccccc',
    paddingBottom: '4%',
  },
  task: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  taskImg: {
    borderRadius: 30,
    overflow: 'hidden',
  },
  taskText: {
    fontWeight: '500',
    color: 'black',
  },
  cardElement: {
    width: '42%',
    height: 150,
    marginLeft: '5%',
    marginTop: '5%',
    borderRadius: 10,
    overflow: 'hidden',
  },
  textColor: {
    color: '#7a7474f5',
  },
});
export default styles;
