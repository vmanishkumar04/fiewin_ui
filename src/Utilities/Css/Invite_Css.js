import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  BackImg: {
    maxHeight: '25%',
    maxWidth: '100%',
  },
  headerCard: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    backgroundColor: '#ffffff',
    width: '90%',
    alignSelf: 'center',
    marginTop: '15%',
    borderRadius: 7,
    paddingVertical: '7%',
    paddingHorizontal: '5%',
  },
  headerCardBtn: {
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    padding: '2%',
    width: '35%',
    backgroundColor: '#fc940d',
  },
  headerCardBtnText: {
    color: '#ffffff',
  },
  B_Card: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    position: 'absolute',
    top: '17%',
    width: '100%',
    height: 105,
  },
  b_Imgcard: {
    height: '100%',
    width: '30%',
    borderRadius: 17,
    overflow: 'hidden',
    position: 'relative',
  },
  bodyImg: {
    marginTop: '3%',
    height: 100,
    width: '100%',
    borderRadius: 10,
    alignSelf: 'center',
  },
  II_container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  II_TextCenter: {
    textAlign: 'center',
    fontWeight: '400',
    color: '#000000',
  },
  //   footer
  footerConatainer: {
    alignItems: 'center',
    marginTop: '3%',
    minHeight: '23%',
    backgroundColor: '#ffffff',
  },

  incomeText: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '5%',
    paddingVertical: '2%',
  },
  inviteBtn: {
    borderRadius: 20,
    backgroundColor: '#fe9a22',
    padding: '3%',
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    marginTop: '2%',
  },
  textColor: {
    color: '#7a7474f5',
  },
});

export default styles;
