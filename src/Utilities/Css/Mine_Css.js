import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  h_t: {
    color: '#000000',
    fontSize: 17,
    fontWeight: '800',
  },
  h_v: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '4%',
    paddingTop: '3%',
    alignItems: 'center',
  },
  h_v_1: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '75%',
  },
  h_to: {
    backgroundColor: 'rgba(0,0,0,0.1)',
    padding: '3%',
    paddingHorizontal: '4%',
    borderRadius: 20,
  },
  //   body
  b_v: {
    marginTop: '5%',
    height: 350,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: 'rgba(0,0,0,0.1)',
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignContent: 'center',
    padding: '3%',
  },
  c_To: {
    position: 'absolute',
    top: '40%',
    width: '30%',
    justifyContent: 'center',
    alignItems: 'center',
    height: '30%',
    backgroundColor: 'rgba(0,255,0,0.3)',
    borderRadius: 50,
  },
  c_Te: {
    backgroundColor: 'rgba(0,255,0,0.7)',
    padding: '20%',
    textAlign: 'center',
    borderRadius: 50,
    paddingVertical: '24%',
    fontSize: 18,
    fontWeight: '800',
  },
  a_b: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '10%',
    paddingVertical: '5%',
  },
  a_b_t: {
    color: '#000000',
    fontSize: 30,
    fontWeight: '900',
  },
  ab_touch: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    width: '20%',
    borderRadius: 50,
    paddingVertical: '4%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  // rule
  rule_T: {
    color: '#000000',
    fontSize: 16,
    fontWeight: '500',
    width: '90%',
    alignSelf: 'center',
    textTransform: 'capitalize',
    textAlign: 'justify',
    marginVertical: '3%',
  },
  rule_Img: {
    width: '80%',
    // height: '80%',
    alignSelf: 'center',
  },
});

export default styles;
