import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  headContainer: {
    backgroundColor: '#ffffff',
  },
  headText: {
    fontSize: 20,
    textAlign: 'center',
    paddingVertical: '3%',
    color: '#000000',
    fontWeight: '700',
    borderBottomWidth: 1,
    borderBottomColor: '#ababab26',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '3%',
    alignItems: 'center',
    padding: '5%',
  },
  headImgContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headImg: {
    height: 60,
    width: 60,
    borderRadius: 50,
    marginRight: '6%',
  },
  detailText: {
    fontSize: 17,
    fontWeight: '400',
    color: '#7a7474f5',
  },
  ModifyText: {
    fontWeight: '600',
    fontSize: 17,
    color: '#7a7474f5',
  },
  // body
  bodyMainContainer: {
    backgroundColor: '#ffffff',
    marginTop: '5%',
  },
  bodyContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: '4%',
    borderBottomWidth: 0.5,
    alignItems: 'center',
    borderBottomColor: '#aaaaaa42',
  },
  bodyImg: {
    height: 40,
    width: 40,
    marginRight: '5%',
  },
  bodyImgContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  div2Text: {
    fontSize: 16,
    fontWeight: '600',
    color: '#7a7474f5',
  },
  //   div 3
  div3MainContainer: {
    backgroundColor: '#ffffff',
    marginTop: '5%',
  },
  signOutText: {
    width: '100%',
    textAlign: 'center',
    marginTop: '2%',
    fontSize: 16,
    fontWeight: '600',
    color: '#7a7474f5',
  },
  //  for all
  arrowFont: {
    fontWeight: '600',
    fontSize: 20,
    color: '#7a7474f5',
  },
});
export default styles;
