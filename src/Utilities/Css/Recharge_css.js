import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    minHeight: '7%',
    borderBottomWidth: 1,
    borderBottomColor: '#aaaaaa78',
  },
  headText: {
    fontSize: 15,
    color: "#7a7474f5"
  },
  bodyBalance: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '3%',
  },
  bodyBalText: {
    fontSize: 17,
    color: "#7a7474f5"
  },
  inpMainContainer: {
    width: '90%',
    alignSelf: 'center',
  },
  inpAmtText: {
    fontSize: 22,
    fontWeight: '700',
    color: '#000000',
  },
  inpContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inpBox: {
    alignSelf: 'center',
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#aaaaaa99',
    fontSize: 20,
    padding: '4%',
    color: "#7a7474f5",
  },
  cardContainer: {
    marginTop: '4%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  cardText: {
    width: '26%',
    backgroundColor: '#e8eaf8',
    marginVertical: '2%',
    padding: '3%',
    alignItems: 'center',
    textAlign: 'center',
    borderRadius: 10,
    marginBottom: '7%',
  },
  amtText: {
    fontWeight: '700',
    fontSize: 16,
    color: "#7a7474f5"
  },
  btn: {
    width: '75%',
    backgroundColor: '#0092ff',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center',
    padding: '4%',
    borderRadius: 10,
  },
  btnText: {
    fontSize: 15,
    fontWeight: '600',
    color: '#ffffff',
  },
  icon: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '60%',
    alignSelf: 'center',
    marginTop: '5%',
  },
  iconImgContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconSize: {
    marginRight: '5%',
    height: 35,
    width: 35,
    borderRadius: 30,
  },
  iconText: {
    fontSize: 15,
    color: "#7a7474f5"
  },
  //   footer
  footerContainer: {
    marginTop: '15%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  footerText: {
    fontSize: 16,
    color: "#7a7474f5"
  },
});
export default styles;
