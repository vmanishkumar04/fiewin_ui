import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  color: {
    color: '#ffffff',
    fontSize: 22,
  },

  head: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '95%',
    overflow: 'hidden',
    paddingVertical: '2%',
  },
  headT: {
    width: '48%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: '4%',
    borderRadius: 15,
    borderBottomWidth: 2,
  },
  headText: {
    fontSize: 20,
    fontWeight: '900',
  },
  InputBox: {
    width: '95%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: '10%',
    textAlign: 'center',
  },
  inp: {
    width: '85%',
    borderRadius: 30,
    paddingHorizontal: '2%',
    fontSize: 20,
    textAlign: 'center',
    backgroundColor: 'rgba(255,255,255,0.3)',
  },
  label: {
    marginTop: '5%',
    width: '80%',
    fontSize: 20,
  },
  btn: {
    width: '60%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '5%',
    borderRadius: 30,
    paddingVertical: 10,
    backgroundColor: '#1161ee',
  },
});

export default styles;
