import {BASE_URL} from '../Constants/ApiConstant';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
export const getFetch = async (url, token = 'krugg') => {
  try {
    const response = await axios({
      url: `${BASE_URL}/${url}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      withCredentials: true,
    });
    if (response.status === 200) {
      return await response.data;
    } else {
      console.log('data not fetch');
    }
  } catch (error) {
    if (error.response.status === 401) {
      return 401;
    } else {
      return error.response;
    }
  }
};
export const getOneFetch = async (url, id) => {
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await axios({
      url: `${BASE_URL}/${url}/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      withCredentials: true,
    });
    if (response.status === 200) {
      return await response.data;
    }
  } catch (error) {
    if (error.response.status === 401) {
      return 401;
    } else {
      return error.response;
    }
  }
};
export const postFetch = async (url, data) => {
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await axios({
      method: 'post',
      url: `${BASE_URL}/${url}`,
      headers: {
        'content-type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
      data: JSON.stringify(data),
      withCredentials: true,
    });
    if (response.status === 200) {
      return await response;
    }
  } catch (error) {
    if (error.response.status === 401) {
      return 401;
    } else {
      return error.response;
    }
  }
};
export const patchFetch = async (url, dataa) => {
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await axios({
      method: 'patch',
      url: `${BASE_URL}/${url}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data: dataa,
      withCredentials: true,
    });
    if (response.status === 200) {
      return await response;
    }
  } catch (error) {
    if (error.response.status === 401) {
      return 401;
    } else {
      return error.response;
    }
  }
};
export const deleteFetch = async (url, id) => {
  try {
    const token = await AsyncStorage.getItem('token');
    const response = await axios({
      method: 'delete',
      url: `${BASE_URL}/${url}/${id}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
      withCredentials: true,
    });
    if (response.status === 200) {
      return await response;
    }
  } catch (error) {
    if (error.response.status === 401) {
      return 401;
    } else {
      error.response;
    }
  }
};
